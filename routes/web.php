<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('/', 'HelloController@index');
Route::get('hello', 'HelloController@index');
Route::get('hello/salesforce', 'HelloController@salesforce');
Route::get('hello/salesforce_list', 'HelloController@salesforce_list');
Route::post('hello/salesforce_list', 'HelloController@salesforce_delete');
Route::get('hello/salesforce_delete', 'HelloController@salesforce_delete');
Route::get('hello/salesforce_update', 'HelloController@salesforce_update');
Route::get('hello/salesforce_edit', 'HelloController@salesforce_edit');
Route::post('hello/salesforce_edit', 'HelloController@salesforce_update');

