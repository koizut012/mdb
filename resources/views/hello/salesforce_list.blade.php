@extends('layouts.helloapp')

@section('content')

@foreach ($records as $record)
<form action="" method="post">
{{ csrf_field() }}
<input type="hidden" name="Id" value="{{$record['Id']}}">
<div>
<div style="float:left;padding-left:10px;">
{{$record['user_name__c']}}
</div>
<div style="float:left;padding-left:10px;">
{{$record['user_email__c']}}
</div>
<div style="float:left;padding-left:10px;">
<a href="{{ action('HelloController@salesforce_edit') }}?Id={{$record['Id']}}">編集</a>
</div>
<div style="float:left;padding-left:10px;">
<input type="submit" value="削除">
</div>
<br style="clear:both">
</div>
</form>
@endforeach

@endsection

@section('footer')
統合システム
@endsection
