@extends('layouts.helloapp')

@section('content')

<form action="" method="post">
{{ csrf_field() }}
<input type="hidden" name="Id" value="{{$record['Id']}}">
<div>
<div>
名前：<input type="text" name="user_name__c" value="{{$record['user_name__c']}}" style="width:600px;">
</div>
<div>
メールアドレス：<input type="text" name="user_email__c" value="{{$record['user_email__c']}}" style="width:600px;">
</div>
<div>
<input type="submit" value="更新">
</div>
<br style="clear:both">
</div>
</form>

@endsection

@section('footer')
統合システム
@endsection
