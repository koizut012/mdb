<html>
<head>
    <title>統合システム</title>
    <style>
    body {font-size:16pt; color:#999; margin: 5px; }
    h1 { font-size:50pt; text-align:right; color:#f6f6f6;
        margin:-20px 0px -30px 0px; letter-spacing:-4pt; }
    ul { font-size:12pt; }
    th {background-color:#999; color:fff; padding:5px 10px; }
    td {border: solid 1px #aaa; color:#999; padding:5px 10px; }
    hr { margin: 25px 100px; border-top: 1px dashed #ddd; }
    .content {margin:10px; }
    .footer { text-align:right; font-size:10pt; margin:10px;
        border-bottom:solid 1px #ccc; color:#ccc; }
        #top_menu li{
            display:inline;
            margin-right:20px;
        }
    </style>
</head>
<body>
    <ul id="top_menu">
        <li><a href="{{ action('HelloController@index') }}">トップ</a></li>
        <li><a href="{{ action('HelloController@salesforce_list') }}">会員一覧</a></li>
        <li><a href="{{ action('HelloController@salesforce') }}">会員追加</a></li>
    </ul>
    <hr size="1">
    <div class="content">
    @yield('content')
    </div>
    <div class="footer">
    @yield('footer')
    </div>
</body>
</html>
