<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMembersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('members', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('family_name', 48)->nullable();
			$table->string('first_name', 48)->nullable();
			$table->string('family_name_k', 48)->nullable();
			$table->string('first_name_k', 48)->nullable();
			$table->date('birthday')->nullable();
			$table->string('email')->nullable();
			$table->string('tel')->nullable();
			$table->dateTime('modified_record')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('members');
	}

}
