<?php

namespace App\Http\Controllers;

use DB;
use App\Http\Controllers\Controller;
use App\Member;
use App\Oauthx;
use Illuminate\Http\Request;

class HelloController extends Controller
{
    
    public function index()
    {
        
        return view('hello.index');
        
        exit();
        
        // バッチ処理の実装をブラウザで確認できるように以下にも記述しています
        // 以下のバッチ処理と同じ内容です
        // app/Console/Commands/IntegrationCommand.php
        
        // 名前（フリガナ）と生年月日と、
        // メールアドレスか電話番号のどちらかが一致するものを重複としています。
        
        // MrMiss
        $rows = DB::connection('mysql_2')->select('SELECT * FROM members;');
        
        foreach($rows as $row){
            
            $name = $row->name;
            $name_kana = $row->name_kana;
            $birthday = $row->birthday;
            $email = $row->email;
            $tel = $row->tel;
            $modified = $row->modified;
            // マッチしたデータを取得
            $Member = new Member;
            $memberData = $Member->getMemberDataMatch($name_kana,$birthday,$email,$tel);
            if( $memberData == false ){
                //インサート
                $Member->insertMemberDataWhenNotMatch($name,$name_kana,$birthday,$email,$tel,$modified);
            }else{
                if( $modified > $memberData->modified_record ){
                    $memberId = $memberData->id;
                    //アップデート
                    $Member->updateMemberDataWhenMatch($memberId,$name,$name_kana,$birthday,$email,$tel,$modified);
                }
            }
            
        }
        
        // デイズ
        $rows = DB::connection('mysql_3')->select('SELECT * FROM members;');
        
        foreach($rows as $row){
            
            $name = $row->name;
            $name_kana = $row->name_kana;
            $birthday = $row->birthday;
            $email = $row->email;
            $tel = $row->tel;
            $modified = $row->modified;
            // マッチしたデータを取得
            $Member = new Member;
            $memberData = $Member->getMemberDataMatch($name_kana,$birthday,$email,$tel);
            if( $memberData == false ){
                //インサート
                $Member->insertMemberDataWhenNotMatch($name,$name_kana,$birthday,$email,$tel,$modified);
            }else{
                if( $modified > $memberData->modified_record ){
                    $memberId = $memberData->id;
                    //アップデート
                    $Member->updateMemberDataWhenMatch($memberId,$name,$name_kana,$birthday,$email,$tel,$modified);
                }
            }
            
        }
        
        // プレミアムステイタス
        $rows = DB::connection('mysql_4')->select('SELECT * FROM member;');
        
        foreach($rows as $row){
            
            $first_name = $row->first_name;
            $second_name = $row->second_name;
            $first_name_k = $row->first_name_k;
            $second_name_k = $row->second_name_k;
            $birth = $row->birth;
            $mail = $row->mail;
            $tel = $row->tel;
            $modified = $row->modified;
            
            // マッチしたデータを取得
            $Member = new Member;
            $memberData = $Member->getMemberDataMatchForPremium($first_name_k,$second_name_k,$birth,$mail,$tel);
            
            if( $memberData == false ){
                //インサート
                $Member->insertMemberDataWhenNotMatchForPremium($first_name,$second_name,$first_name_k,$second_name_k,$birth,$mail,$tel,$modified);
            }else{
                if( $modified > $memberData->modified_record ){
                    $memberId = $memberData->id;
                    //アップデート
                    $Member->updateMemberDataWhenMatchForPremium($memberId,$first_name,$second_name,$first_name_k,$second_name_k,$birth,$mail,$tel,$modified);
                }
            }
            
        }
        
        // 自衛隊
        $rows = DB::connection('mysql_5')->select('SELECT * FROM members;');
        
        foreach($rows as $row){
            
            $name = $row->name;
            $name_kana = $row->name_kana;
            $birthday = $row->birthday;
            $email = $row->email;
            $tel = $row->tel;
            $modified = $row->modified;
            // マッチしたデータを取得
            $Member = new Member;
            $memberData = $Member->getMemberDataMatch($name_kana,$birthday,$email,$tel);
            if( $memberData == false ){
                //インサート
                $Member->insertMemberDataWhenNotMatch($name,$name_kana,$birthday,$email,$tel,$modified);
            }else{
                if( $modified > $memberData->modified_record ){
                    $memberId = $memberData->id;
                    //アップデート
                    $Member->updateMemberDataWhenMatch($memberId,$name,$name_kana,$birthday,$email,$tel,$modified);
                }
            }
            
        }
        
        // ホワイトパートナーズ
        $rows = DB::connection('mysql_6')->select('SELECT * FROM members;');
        
        foreach($rows as $row){
            
            $name = $row->name;
            $name_kana = $row->name_kana;
            $birthday = $row->birthday;
            $email = $row->email;
            $tel = $row->tel;
            $modified = $row->modified;
            // マッチしたデータを取得
            $Member = new Member;
            $memberData = $Member->getMemberDataMatch($name_kana,$birthday,$email,$tel);
            if( $memberData == false ){
                //インサート
                $Member->insertMemberDataWhenNotMatch($name,$name_kana,$birthday,$email,$tel,$modified);
            }else{
                if( $modified > $memberData->modified_record ){
                    $memberId = $memberData->id;
                    //アップデート
                    $Member->updateMemberDataWhenMatch($memberId,$name,$name_kana,$birthday,$email,$tel,$modified);
                }
            }
            
        }
        
        exit();
        
    }
    
    /*
     * salesforceにレコード登録
     */
    public function salesforce(){
        
        // 以下のソースコードは以下のページのものを改造しています
        // https://qiita.com/na0AaooQ/items/157c28a80948c4b97bed
        
        date_default_timezone_set('Asia/Tokyo');
        
        // Salesforce REST APIで接続するアプリケーションの「コンシューマ鍵」
        $DATABASEDOTCOM_CLIENT_ID = "3MVG9YDQS5WtC11qew9kr9i7LEvBe3f0.ZnV2qfG0FtDwO9n_04XDdk6k8hGQGN7yK7zOlX7G7JjdgLLpalGa";
        
        // Salesforce REST APIで接続するアプリケーションの「コンシューマの秘密」
        $DATABASEDOTCOM_CLIENT_SECRET = "5337687511178078585";
        
        // Salesforce REST API接続用のSalesforceユーザ(Salesforceログインに使用しているEメールアドレスを設定)
        $DATABASEDOTCOM_CLIENT_USERNAME = "minamikawa@gmail.com";
        
        // Salesforce REST API接続用のSalesforceユーザのパスワード(後ろにAPI セキュリティトークンを追記)
        $DATABASEDOTCOM_CLIENT_AUTHENTICATE_PASSWORD = "pass0619"."LWsHU6pK1LZcmhFc0TB5M05y";
        
        // Salesforce REST APIエンドポイント
        $DATABASEDOTCOM_HOST = "login.salesforce.com";
        $LOGIN_URL = "https://" . $DATABASEDOTCOM_HOST . "/";
        
        ///// Salesforce REST API接続用の設定
        $CACHE_DIR = $_SERVER['DOCUMENT_ROOT']."/tmp/session";
        $CALLBACK_URL = 'https://master.fandr.jp/public/hello/salesforce';
        
        /////
        // Salesforce REST API接続用のOauthインスタンスを生成
        $oauth = new Oauthx( $DATABASEDOTCOM_CLIENT_ID, $DATABASEDOTCOM_CLIENT_SECRET, $CALLBACK_URL, $LOGIN_URL, $CACHE_DIR);
        
        // Salesforce REST API接続にあたりSalesforceへの認証を実行
        $oauth->auth_with_password( $DATABASEDOTCOM_CLIENT_USERNAME, $DATABASEDOTCOM_CLIENT_AUTHENTICATE_PASSWORD );
        
        // Salesforce REST APIでカスタムオブジェクトへ追加するレコード
        $insert_time = date("Ymd_His");
        
        $insert_name = "testname_" . $insert_time;
        $insert_email = $insert_name . "@hoge.example.com";
        $insert_record = array(
            'user_name__c' => $insert_name,
            'user_email__c' => $insert_email,
        );
        
        $insert_record_json = json_encode($insert_record);
        $insert_record_json = preg_replace('/\\\\\//', '/', $insert_record_json);
        
        $url = $oauth->instance_url . "/services/data/v20.0/sobjects/QuickStart__c";
        $curl = curl_init($url);
        
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, TRUE);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $insert_record_json);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json;charset=UTF-8', "Authorization: OAuth " . $oauth->access_token));
        
        $response = json_decode(curl_exec($curl), true);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        
        // insert成功時は201が返ってくるので、201以外はエラーと判定
        if ( $status != 201 ) {
            print("Salesforce REST API Access Failed  StatusCode =[" . $status . "]\n");
        } else {
            print("Salesforce REST API Access Success StatusCode =[" . $status . "]\n");
        }
        
        curl_close($curl);
        
        $oauth->auth_with_refresh_token();
        
        return redirect('/hello/salesforce_list');
        
    }
    
    /*
     * salesforceからレコードを取得
     */
    public function salesforce_list(){
        
        // 以下のソースコードは以下のページのものを改造しています
        // https://qiita.com/na0AaooQ/items/157c28a80948c4b97bed
        
        // Salesforce REST APIで接続するアプリケーションの「コンシューマ鍵」
        $DATABASEDOTCOM_CLIENT_ID = "3MVG9YDQS5WtC11qew9kr9i7LEvBe3f0.ZnV2qfG0FtDwO9n_04XDdk6k8hGQGN7yK7zOlX7G7JjdgLLpalGa";
        
        // Salesforce REST APIで接続するアプリケーションの「コンシューマの秘密」
        $DATABASEDOTCOM_CLIENT_SECRET = "5337687511178078585";
        
        // Salesforce REST API接続用のSalesforceユーザ(Salesforceログインに使用しているEメールアドレスを設定)
        $DATABASEDOTCOM_CLIENT_USERNAME = "minamikawa@gmail.com";
        
        // Salesforce REST API接続用のSalesforceユーザのパスワード(後ろにAPI セキュリティトークンを追記)
        $DATABASEDOTCOM_CLIENT_AUTHENTICATE_PASSWORD = "pass0619"."LWsHU6pK1LZcmhFc0TB5M05y";
        
        // Salesforce REST APIエンドポイントを.bashrcから取得する
        $DATABASEDOTCOM_HOST = "login.salesforce.com";
        $LOGIN_URL = "https://" . $DATABASEDOTCOM_HOST . "/";
        
        ///// Salesforce REST API接続用の設定
        $CACHE_DIR = $_SERVER['DOCUMENT_ROOT']."/tmp/session";
        $CALLBACK_URL = 'https://master.fandr.jp/public/hello/salesforce';
        
        /////
        // Salesforce REST API接続用のOauthインスタンスを生成
        $oauth = new Oauthx( $DATABASEDOTCOM_CLIENT_ID, $DATABASEDOTCOM_CLIENT_SECRET, $CALLBACK_URL, $LOGIN_URL, $CACHE_DIR);
        
        // Salesforce REST API接続にあたりSalesforceへの認証を実行
        $oauth->auth_with_password( $DATABASEDOTCOM_CLIENT_USERNAME, $DATABASEDOTCOM_CLIENT_AUTHENTICATE_PASSWORD );
        
        // Salesforce REST APIパラメータに指定するSOQLを定義
        $query = "SELECT id,user_name__c, user_email__c FROM QuickStart__c";
        
        // Salesforce REST APIを実行する
        $url = $oauth->instance_url . "/services/data/v24.0/query?q=" . urlencode($query);
        $curl = curl_init($url);
        
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: OAuth " . $oauth->access_token));
        
        // Salesforce REST API実行結果を保存
        $response = json_decode(curl_exec($curl), true);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        
        curl_close($curl);
        
        $oauth->auth_with_refresh_token();
        
        $records = $response['records'];
        
        $param = ['records' => $records];
        return view('hello.salesforce_list', $param);
        
    }
    
    /*
     * salesforceのレコードを削除
     */
    public function salesforce_delete(Request $request){
        
        date_default_timezone_set('Asia/Tokyo');
        
        // Salesforce REST APIで接続するアプリケーションの「コンシューマ鍵」
        $DATABASEDOTCOM_CLIENT_ID = "3MVG9YDQS5WtC11qew9kr9i7LEvBe3f0.ZnV2qfG0FtDwO9n_04XDdk6k8hGQGN7yK7zOlX7G7JjdgLLpalGa";
        
        // Salesforce REST APIで接続するアプリケーションの「コンシューマの秘密」
        $DATABASEDOTCOM_CLIENT_SECRET = "5337687511178078585";
        
        // Salesforce REST API接続用のSalesforceユーザ(Salesforceログインに使用しているEメールアドレスを設定)
        $DATABASEDOTCOM_CLIENT_USERNAME = "minamikawa@gmail.com";
        
        // Salesforce REST API接続用のSalesforceユーザのパスワード(後ろにAPI セキュリティトークンを追記)
        $DATABASEDOTCOM_CLIENT_AUTHENTICATE_PASSWORD = "pass0619"."LWsHU6pK1LZcmhFc0TB5M05y";
        
        // Salesforce REST APIエンドポイント
        $DATABASEDOTCOM_HOST = "login.salesforce.com";
        $LOGIN_URL = "https://" . $DATABASEDOTCOM_HOST . "/";
        
        ///// Salesforce REST API接続用の設定
        $CACHE_DIR = $_SERVER['DOCUMENT_ROOT']."/tmp/session";
        $CALLBACK_URL = 'https://master.fandr.jp/public/hello/salesforce';
        
        /////
        // Salesforce REST API接続用のOauthインスタンスを生成
        $oauth = new Oauthx( $DATABASEDOTCOM_CLIENT_ID, $DATABASEDOTCOM_CLIENT_SECRET, $CALLBACK_URL, $LOGIN_URL, $CACHE_DIR);
        
        // Salesforce REST API接続にあたりSalesforceへの認証を実行
        $oauth->auth_with_password( $DATABASEDOTCOM_CLIENT_USERNAME, $DATABASEDOTCOM_CLIENT_AUTHENTICATE_PASSWORD );
        
        $url = $oauth->instance_url . "/services/data/v20.0/sobjects/QuickStart__c/".$request->Id;
        $curl = curl_init($url);
        
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json;charset=UTF-8', "Authorization: Bearer " . $oauth->access_token));
        
        $response = json_decode(curl_exec($curl), true);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        
        curl_close($curl);
        
        $oauth->auth_with_refresh_token();
        
        return view('hello.salesforce_delete');
        
    }
    
    /*
     * salesforceのレコードを更新
     */
    public function salesforce_update(Request $request){
        
        $Id = $request->Id;
        $user_name__c = $request->user_name__c;
        $user_email__c = $request->user_email__c;
        
        $content = json_encode(array("user_name__c" => $user_name__c,"user_email__c" => $user_email__c));
        
        date_default_timezone_set('Asia/Tokyo');
        
        // Salesforce REST APIで接続するアプリケーションの「コンシューマ鍵」
        $DATABASEDOTCOM_CLIENT_ID = "3MVG9YDQS5WtC11qew9kr9i7LEvBe3f0.ZnV2qfG0FtDwO9n_04XDdk6k8hGQGN7yK7zOlX7G7JjdgLLpalGa";
        
        // Salesforce REST APIで接続するアプリケーションの「コンシューマの秘密」
        $DATABASEDOTCOM_CLIENT_SECRET = "5337687511178078585";
        
        // Salesforce REST API接続用のSalesforceユーザ(Salesforceログインに使用しているEメールアドレスを設定)
        $DATABASEDOTCOM_CLIENT_USERNAME = "minamikawa@gmail.com";
        
        // Salesforce REST API接続用のSalesforceユーザのパスワード(後ろにAPI セキュリティトークンを追記)
        $DATABASEDOTCOM_CLIENT_AUTHENTICATE_PASSWORD = "pass0619"."LWsHU6pK1LZcmhFc0TB5M05y";
        
        // Salesforce REST APIエンドポイント
        $DATABASEDOTCOM_HOST = "login.salesforce.com";
        $LOGIN_URL = "https://" . $DATABASEDOTCOM_HOST . "/";
        
        ///// Salesforce REST API接続用の設定
        $CACHE_DIR = $_SERVER['DOCUMENT_ROOT']."/tmp/session";
        $CALLBACK_URL = 'https://master.fandr.jp/public/hello/salesforce';
        
        /////
        // Salesforce REST API接続用のOauthインスタンスを生成
        $oauth = new Oauthx( $DATABASEDOTCOM_CLIENT_ID, $DATABASEDOTCOM_CLIENT_SECRET, $CALLBACK_URL, $LOGIN_URL, $CACHE_DIR);
        
        // Salesforce REST API接続にあたりSalesforceへの認証を実行
        $oauth->auth_with_password( $DATABASEDOTCOM_CLIENT_USERNAME, $DATABASEDOTCOM_CLIENT_AUTHENTICATE_PASSWORD );
        
        $url = $oauth->instance_url . "/services/data/v20.0/sobjects/QuickStart__c/".$Id;
        $curl = curl_init($url);
        
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json;charset=UTF-8', "Authorization: OAuth " . $oauth->access_token));
        
        $response = json_decode(curl_exec($curl), true);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        
        curl_close($curl);
        
        $oauth->auth_with_refresh_token();
        
        return redirect('/hello/salesforce_list');
        
    }
    
    /*
     * salesforceのレコードを編集
     */
    public function salesforce_edit(){
        
        $Id = $_GET["Id"];
        
        // Salesforce REST APIで接続するアプリケーションの「コンシューマ鍵」
        $DATABASEDOTCOM_CLIENT_ID = "3MVG9YDQS5WtC11qew9kr9i7LEvBe3f0.ZnV2qfG0FtDwO9n_04XDdk6k8hGQGN7yK7zOlX7G7JjdgLLpalGa";
        
        // Salesforce REST APIで接続するアプリケーションの「コンシューマの秘密」
        $DATABASEDOTCOM_CLIENT_SECRET = "5337687511178078585";
        
        // Salesforce REST API接続用のSalesforceユーザ(Salesforceログインに使用しているEメールアドレスを設定)
        $DATABASEDOTCOM_CLIENT_USERNAME = "minamikawa@gmail.com";
        
        // Salesforce REST API接続用のSalesforceユーザのパスワード(後ろにAPI セキュリティトークンを追記)
        $DATABASEDOTCOM_CLIENT_AUTHENTICATE_PASSWORD = "pass0619"."LWsHU6pK1LZcmhFc0TB5M05y";
        
        // Salesforce REST APIエンドポイントを.bashrcから取得する
        $DATABASEDOTCOM_HOST = "login.salesforce.com";
        $LOGIN_URL = "https://" . $DATABASEDOTCOM_HOST . "/";
        
        ///// Salesforce REST API接続用の設定
        $CACHE_DIR = $_SERVER['DOCUMENT_ROOT']."/tmp/session";
        $CALLBACK_URL = 'https://master.fandr.jp/public/hello/salesforce';
        
        /////
        // Salesforce REST API接続用のOauthインスタンスを生成
        $oauth = new Oauthx( $DATABASEDOTCOM_CLIENT_ID, $DATABASEDOTCOM_CLIENT_SECRET, $CALLBACK_URL, $LOGIN_URL, $CACHE_DIR);
        
        // Salesforce REST API接続にあたりSalesforceへの認証を実行
        $oauth->auth_with_password( $DATABASEDOTCOM_CLIENT_USERNAME, $DATABASEDOTCOM_CLIENT_AUTHENTICATE_PASSWORD );
        
        // Salesforce REST APIパラメータに指定するSOQLを定義
        $query = sprintf("SELECT id,user_name__c, user_email__c FROM QuickStart__c WHERE Id='%s'",$Id);
        
        // Salesforce REST APIを実行する
        $url = $oauth->instance_url . "/services/data/v24.0/query?q=" . urlencode($query);
        $curl = curl_init($url);
        
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: OAuth " . $oauth->access_token));
        
        // Salesforce REST API実行結果を保存
        $response = json_decode(curl_exec($curl), true);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        
        curl_close($curl);
        
        $oauth->auth_with_refresh_token();
        
        $record = $response['records'][0];
        
        $param = ['record' => $record];
        return view('hello.salesforce_edit', $param);
        
    }
    
    
    
    
    
    
    
    
    
    
    
}