<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Util;

class Member extends Model
{
    
    //protected $connection = 'mysql_1';
    
    public function allDataDump(){
        
        $items = $this::all();
        
        var_dump($items);
        exit();
        
    }
    
    public function getMemberDataMatch($name_kana,$birthday,$email,$tel){
        
        $name_kana = trim($name_kana);
        $birthday = trim($birthday);
        $email = trim($email);
        $tel = trim($tel);
        
        $Util = new Util;
        $tmp = $Util->nameSplit($name_kana);
        $family_name_k = isset($tmp[0]) ? $tmp[0] : "";
        $first_name_k = isset($tmp[1]) ? $tmp[1] : "";
        
        $items = $this::all();
        
        foreach($items as $item){
            
            if( ( $item->family_name_k == $family_name_k ) && ( $item->first_name_k == $first_name_k ) && ( $item->birthday == $birthday ) ){
                
                if( ( $item->email == $email ) || ( $item->tel == $tel ) ){
                    
                    return $item;
                    
                }
                
            }
        }
        
        return false;
        
    }
    
    public function insertMemberDataWhenNotMatch($name,$name_kana,$birthday,$email,$tel,$modified){
        
        $name = trim($name);
        $name_kana = trim($name_kana);
        $birthday = trim($birthday);
        $email = trim($email);
        $tel = trim($tel);
        $modified = trim($modified);
        
        $Util = new Util;
        
        $tmp = $Util->nameSplit($name);
        $family_name = isset($tmp[0]) ? $tmp[0] : "";
        $first_name = isset($tmp[1]) ? $tmp[1] : "";
        
        $tmp = $Util->nameSplit($name_kana);
        $family_name_k = isset($tmp[0]) ? $tmp[0] : "";
        $first_name_k = isset($tmp[1]) ? $tmp[1] : "";
        
        // NULLがある場合はinsertしない
        $data[0] = $family_name;
        $data[1] = $first_name;
        $data[2] = $family_name_k;
        $data[3] = $first_name_k;
        $data[4] = $birthday;
        $data[5] = $email;
        $data[6] = $tel;
        $data[7] = $modified;
        $result = $Util->checkNotNullForMemberInsert($data);
        if( $result == false ){
            return false;
        }
        
        $this->family_name = $family_name;
        $this->first_name = $first_name;
        $this->family_name_k = $family_name_k;
        $this->first_name_k = $first_name_k;
        $this->birthday = $birthday;
        $this->email = $email;
        $this->tel = $tel;
        $this->modified_record = $modified;
        
        $this->save();
        
        return true;
        
    }
    
    public function updateMemberDataWhenMatch($memberId,$name,$name_kana,$birthday,$email,$tel,$modified){
        
        $name = trim($name);
        $name_kana = trim($name_kana);
        $birthday = trim($birthday);
        $email = trim($email);
        $tel = trim($tel);
        $modified = trim($modified);
        
        $Util = new Util;
        
        $tmp = $Util->nameSplit($name);
        $family_name = isset($tmp[0]) ? $tmp[0] : "";
        $first_name = isset($tmp[1]) ? $tmp[1] : "";
        
        $tmp = $Util->nameSplit($name_kana);
        $family_name_k = isset($tmp[0]) ? $tmp[0] : "";
        $first_name_k = isset($tmp[1]) ? $tmp[1] : "";
        
        $data = $this::find($memberId);
        
        $data->family_name = $family_name;
        $data->first_name = $first_name;
        $data->family_name_k = $family_name_k;
        $data->first_name_k = $first_name_k;
        $data->birthday = $birthday;
        $data->email = $email;
        $data->tel = $tel;
        $data->modified_record = $modified;
        
        $data->save();
        
        return true;
        
    }
    
    public function getMemberDataMatchForPremium($first_name_k,$second_name_k,$birth,$mail,$tel){
        
        $family_name_k = trim($first_name_k);
        $first_name_k = trim($second_name_k);
        $birth = trim($birth);
        $email = trim($mail);
        $tel = trim($tel);
        
        $Util = new Util;
        $birthday = $Util->getBirthdayStr($birth);
        
        $items = $this::all();
        
        foreach($items as $item){
            
            if( ( $item->family_name_k == $family_name_k ) && ( $item->first_name_k == $first_name_k ) && ( $item->birthday == $birthday ) ){
                
                if( ( $item->email == $email ) || ( $item->tel == $tel ) ){
                    
                    return $item;
                    
                }
                
            }
        }
        
        return false;
        
    }
    
    public function insertMemberDataWhenNotMatchForPremium($first_name,$second_name,$first_name_k,$second_name_k,$birth,$mail,$tel,$modified){
        
        $family_name = trim($first_name);
        $first_name = trim($second_name);
        $family_name_k = trim($first_name_k);
        $first_name_k = trim($second_name_k);
        $birth = trim($birth);
        $email = trim($mail);
        $tel = trim($tel);
        $modified = trim($modified);
        
        $Util = new Util;
        $birthday = $Util->getBirthdayStr($birth);
        
        // NULLがある場合はinsertしない
        $data[0] = $family_name;
        $data[1] = $first_name;
        $data[2] = $family_name_k;
        $data[3] = $first_name_k;
        $data[4] = $birthday;
        $data[5] = $email;
        $data[6] = $tel;
        $data[7] = $modified;
        $result = $Util->checkNotNullForMemberInsert($data);
        if( $result == false ){
            return false;
        }
        
        $this->family_name = $family_name;
        $this->first_name = $first_name;
        $this->family_name_k = $family_name_k;
        $this->first_name_k = $first_name_k;
        $this->birthday = $birthday;
        $this->email = $email;
        $this->tel = $tel;
        $this->modified_record = $modified;
        
        $this->save();
        
        return true;
        
    }
    
    public function updateMemberDataWhenMatchForPremium($memberId,$first_name,$second_name,$first_name_k,$second_name_k,$birth,$mail,$tel,$modified){
        
        $family_name = trim($first_name);
        $first_name = trim($second_name);
        $family_name_k = trim($first_name_k);
        $first_name_k = trim($second_name_k);
        $birth = trim($birth);
        $email = trim($mail);
        $tel = trim($tel);
        $modified = trim($modified);
        
        $Util = new Util;
        $birthday = $Util->getBirthdayStr($birth);
        
        $data = $this::find($memberId);
        
        $data->family_name = $family_name;
        $data->first_name = $first_name;
        $data->family_name_k = $family_name_k;
        $data->first_name_k = $first_name_k;
        $data->birthday = $birthday;
        $data->email = $email;
        $data->tel = $tel;
        $data->modified_record = $modified;
        
        $data->save();
        
        return true;
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
}
