<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use DB;
use App\Member;

class IntegrationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:integration';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // 名前（フリガナ）と生年月日と、
        // メールアドレスか電話番号のどちらかが一致するものを重複としています。
        
        // MrMiss
        $rows = DB::connection('mysql_2')->select('SELECT * FROM members;');
        
        foreach($rows as $row){
            
            $name = $row->name;
            $name_kana = $row->name_kana;
            $birthday = $row->birthday;
            $email = $row->email;
            $tel = $row->tel;
            $modified = $row->modified;
            // マッチしたデータを取得
            $Member = new Member;
            $memberData = $Member->getMemberDataMatch($name_kana,$birthday,$email,$tel);
            if( $memberData == false ){
                //インサート
                $Member->insertMemberDataWhenNotMatch($name,$name_kana,$birthday,$email,$tel,$modified);
            }else{
                if( $modified > $memberData->modified_record ){
                    $memberId = $memberData->id;
                    //アップデート
                    $Member->updateMemberDataWhenMatch($memberId,$name,$name_kana,$birthday,$email,$tel,$modified);
                }
            }
            
        }
        
        // デイズ
        $rows = DB::connection('mysql_3')->select('SELECT * FROM members;');
        
        foreach($rows as $row){
            
            $name = $row->name;
            $name_kana = $row->name_kana;
            $birthday = $row->birthday;
            $email = $row->email;
            $tel = $row->tel;
            $modified = $row->modified;
            // マッチしたデータを取得
            $Member = new Member;
            $memberData = $Member->getMemberDataMatch($name_kana,$birthday,$email,$tel);
            if( $memberData == false ){
                //インサート
                $Member->insertMemberDataWhenNotMatch($name,$name_kana,$birthday,$email,$tel,$modified);
            }else{
                if( $modified > $memberData->modified_record ){
                    $memberId = $memberData->id;
                    //アップデート
                    $Member->updateMemberDataWhenMatch($memberId,$name,$name_kana,$birthday,$email,$tel,$modified);
                }
            }
            
        }
        
        // プレミアムステイタス
        $rows = DB::connection('mysql_4')->select('SELECT * FROM member;');
        
        foreach($rows as $row){
            
            $first_name = $row->first_name;
            $second_name = $row->second_name;
            $first_name_k = $row->first_name_k;
            $second_name_k = $row->second_name_k;
            $birth = $row->birth;
            $mail = $row->mail;
            $tel = $row->tel;
            $modified = $row->modified;
            
            // マッチしたデータを取得
            $Member = new Member;
            $memberData = $Member->getMemberDataMatchForPremium($first_name_k,$second_name_k,$birth,$mail,$tel);
            
            if( $memberData == false ){
                //インサート
                $Member->insertMemberDataWhenNotMatchForPremium($first_name,$second_name,$first_name_k,$second_name_k,$birth,$mail,$tel,$modified);
            }else{
                if( $modified > $memberData->modified_record ){
                    $memberId = $memberData->id;
                    //アップデート
                    $Member->updateMemberDataWhenMatchForPremium($memberId,$first_name,$second_name,$first_name_k,$second_name_k,$birth,$mail,$tel,$modified);
                }
            }
            
        }
        
        // 自衛隊
        $rows = DB::connection('mysql_5')->select('SELECT * FROM members;');
        
        foreach($rows as $row){
            
            $name = $row->name;
            $name_kana = $row->name_kana;
            $birthday = $row->birthday;
            $email = $row->email;
            $tel = $row->tel;
            $modified = $row->modified;
            // マッチしたデータを取得
            $Member = new Member;
            $memberData = $Member->getMemberDataMatch($name_kana,$birthday,$email,$tel);
            if( $memberData == false ){
                //インサート
                $Member->insertMemberDataWhenNotMatch($name,$name_kana,$birthday,$email,$tel,$modified);
            }else{
                if( $modified > $memberData->modified_record ){
                    $memberId = $memberData->id;
                    //アップデート
                    $Member->updateMemberDataWhenMatch($memberId,$name,$name_kana,$birthday,$email,$tel,$modified);
                }
            }
            
        }
        
        // ホワイトパートナーズ
        $rows = DB::connection('mysql_6')->select('SELECT * FROM members;');
        
        foreach($rows as $row){
            
            $name = $row->name;
            $name_kana = $row->name_kana;
            $birthday = $row->birthday;
            $email = $row->email;
            $tel = $row->tel;
            $modified = $row->modified;
            // マッチしたデータを取得
            $Member = new Member;
            $memberData = $Member->getMemberDataMatch($name_kana,$birthday,$email,$tel);
            if( $memberData == false ){
                //インサート
                $Member->insertMemberDataWhenNotMatch($name,$name_kana,$birthday,$email,$tel,$modified);
            }else{
                if( $modified > $memberData->modified_record ){
                    $memberId = $memberData->id;
                    //アップデート
                    $Member->updateMemberDataWhenMatch($memberId,$name,$name_kana,$birthday,$email,$tel,$modified);
                }
            }
            
        }
        
        exit();
        
    }
}
