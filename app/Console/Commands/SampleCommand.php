<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class SampleCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:sample';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command Sample description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //echo "sample!";
        $rows = DB::connection('mysql_1')->select('SELECT * FROM members LIMIT 1;');
        echo $rows[0]->name;exit();
    }
}
