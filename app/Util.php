<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Util extends Model
{

    public function test()
    {
        echo "util-test!";exit();
    }
    
    public function nameSplit($data)
    {
        $data = explode(" ",$data);
        
        return $data;
        
    }
    
    public function getBirthdayStr($birth){
        
        $year = substr($birth, 0, 4);
        $month = substr($birth, 4, 2);
        $day = substr($birth, 6, 2);
        
        $birthday = $year."-".$month."-".$day;
        
        return $birthday;
        
    }
    
    public function checkNotNullForMemberInsert($data){
        
        $dataNum = count($data);
        
        for( $i=0; $i<$dataNum; $i++ ){
            $tmp = $data[$i];
            
            if( $tmp == NULL ){
                return false;
            }
        }
        
        return true;
        
    }

}
